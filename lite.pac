// WuandLite PAC Rule
function regExpMatch(url, pattern) {    try { return new RegExp(pattern).test(url); } catch(ex) { return false; }    }
function FindProxyForURL(url, host) {
// youku
if (
  regExpMatch(url, "http:\/\/v\.youku\.com\/crossdomain\.xml") ||
  regExpMatch(url, "^.*?://.*?(tudou|qiyi|iqiyi|le|letv|letvcdn|sohu|ku6|ku6cdn|pps).(com|tv)/crossdomain.xml$")
 ) return "PROXY yk.pp.navi.youku.com:80";
// Proxy
if (
// Google
  shExpMatch(url, "*://*.google.*/*") ||
  dnsDomainIs(host, "google.com.hk") ||
  dnsDomainIs(host, "google.co.jp") ||
  dnsDomainIs(host, "gstatic.com") ||
  dnsDomainIs(host, "googleusercontent.com") ||
  dnsDomainIs(host, "ggpht.com") ||
  dnsDomainIs(host, "googleapis.com") ||
  dnsDomainIs(host, "goo.gl") ||
  dnsDomainIs(host, "android.com") ||
  dnsDomainIs(host, "g.co") ||
  dnsDomainIs(host, "gmail.com") ||
  dnsDomainIs(host, "googlemail.com") ||
  dnsDomainIs(host, "golang.org") ||
  dnsDomainIs(host, "appspot.com") ||
  dnsDomainIs(host, "googledrive.com") ||
  dnsDomainIs(host, "feedburner.com") ||
  dnsDomainIs(host, "abc.xyz") ||
  dnsDomainIs(host, "googletagmanager.com") ||
  dnsDomainIs(host, "googlesyndication.com") ||
  dnsDomainIs(host, "googleadservices.com") ||
  dnsDomainIs(host, "doubleclick.net") ||
  dnsDomainIs(host, "google-analytics.com") ||
  dnsDomainIs(host, "chromium.org") ||
  dnsDomainIs(host, "accounts.youtube.com") ||
  dnsDomainIs(host, "gvt1.com") ||
// Wikipedia
  dnsDomainIs(host, "wikipedia.org") ||
  dnsDomainIs(host, "wikimedia.org") ||
// Github
  dnsDomainIs(host, "github.com") ||
  dnsDomainIs(host, "amazonaws.com") ||
  dnsDomainIs(host, "githubusercontent.com") ||
  dnsDomainIs(host, "githubapp.com") ||
// ACG
  dnsDomainIs(host, "gamer.com.tw") ||
  dnsDomainIs(host, "bahamut.com.tw") ||
  dnsDomainIs(host, "pixiv.net") ||
  dnsDomainIs(host, "dmhy.org") ||
  dnsDomainIs(host, "deviantart.com") ||
  dnsDomainIs(host, "deviantart.net") ||
  shExpMatch(url, "http://pics.dmm.co.jp/digital/*48/*") ||
  dnsDomainIs(host, "nicovideo.jp") ||
  dnsDomainIs(host, "smilevideo.jp") ||
  dnsDomainIs(host, "nimg.jp") ||
  dnsDomainIs(host, "dmm.com") ||
  shExpMatch(url, "http://203.104.209.*/*") ||
  shExpMatch(url, "http://125.6.187.*/*") ||
  dnsDomainIs(host, "wikiwiki.jp") ||
  dnsDomainIs(host, "swordlogic.com") ||  
  dnsDomainIs(host, "touken-ranbu.jp") ||
  dnsDomainIs(host, "cxpublic.com") ||
// Other
  dnsDomainIs(host, "economist.com") ||
  dnsDomainIs(host, "adblockplus.org") ||
// Test
  dnsDomainIs(host, "ip.cn")
) return "PROXY 127.0.0.1:8087";
return "DIRECT";
}
